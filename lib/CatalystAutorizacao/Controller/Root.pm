package CatalystAutorizacao::Controller::Root;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=encoding utf-8

=head1 NAME

CatalystAutorizacao::Controller::Root - Root Controller for CatalystAutorizacao

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    if ($c->user_exists) {
        $c->response->body('vc estah logado. <a href="/logout">deslogar</a>');
    }
    else {
        $c->response->body('vc nao estah logado.');
    }
}

sub login :Path('login') Args(2) {
    my ($self, $c, $login, $senha) = @_;
    $c->authenticate({ username => $login, password => $senha });
    $c->res->redirect($c->uri_for_action('/index'));
    $c->detach;
}

sub logout :Path('logout') Args(0) {
    my ($self, $c) = @_;
    $c->logout;
    $c->res->redirect($c->uri_for_action('/index'));
    $c->detach;
}


=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {}

=head1 AUTHOR

sgrs,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
