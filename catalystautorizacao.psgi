use strict;
use warnings;

use CatalystAutorizacao;

my $app = CatalystAutorizacao->apply_default_middlewares(CatalystAutorizacao->psgi_app);

use Plack::Builder;

builder {
    enable 'Compile' => (
        pattern => qr{\.coffee$},
        lib     => 'root',
        blib    => 'root',
        mime    => 'text/plain',
        map     => sub {
            my $filename = shift;
            $filename =~ s/coffee$/js/;
            return $filename;
        },
        compile => sub {
            my ($in, $out) = @_;
            print "compiling $in $out\n\n";
            system("coffee --compile --stdio < $in > $out");
        },
    );

    enable 'Compile' => (
        pattern => qr{\.scss$},
        lib     => 'root',
        blib    => 'root',
        mime    => 'text/plain',
        map     => sub {
            my $filename = shift;
            $filename =~ s/scss$/css/;
            return $filename;
        },
        compile => sub {
            my ($in, $out) = @_;
            print "compiling $in $out\n\n";
            system("sass $in $out");
        },
    );

    $app;
};
